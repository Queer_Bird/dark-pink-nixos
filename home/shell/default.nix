{config, ...}: 
{
imports = [
    ./cli.nix
    ./starship.nix
    ./zsh.nix
  ];
}
