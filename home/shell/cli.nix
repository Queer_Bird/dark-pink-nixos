{
  pkgs,
  config,
  ...
}: {
  home.packages = with pkgs; [
    # archives
    joshuto
    
    zip
    unzip
    unrar
    p7zip
    atool

    neofetch

    # utils
    du-dust
    less
    duf
    fd
    file
    jaq
    ripgrep
    trash-cli
    
    pass
    passExtensions.pass-import
  ];

  programs = {
    bat = {
      enable = true;
      config = {
        pager = "less -FR";
      };    
    };
   };
}
