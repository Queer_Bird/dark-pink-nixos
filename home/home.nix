{ config, pkgs, ... }:

{

  imports = [
	./programs
	./shell
  ];

  home.username = "cmde";
  home.homeDirectory = "/home/cmde";


  # basic configuration of git, please change to your own
  programs.git = {
    enable = true;
    userName = "queer_bird";
    userEmail = "pokingvictim@protonmail.com";
  };

  # starship - an customizable prompt for any shell
  programs.starship = {
    enable = true;
    # custom settings
    settings = {
      add_newline = false;
      aws.disabled = true;
      gcloud.disabled = true;
      line_break.disabled = true;
    };
  };

  programs.bash = {
    enable = true;
    enableCompletion = true;
  };
  


  # This value determines the home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update home Manager without changing this value. See
  # the home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "23.11";

  # Let home Manager install and manage itself.
  programs.home-manager.enable = true;
}
