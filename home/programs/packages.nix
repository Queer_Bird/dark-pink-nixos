{pkgs, ...}: {
  home.packages = with pkgs; [
    
    tdesktop
    nicotine-plus
    lollypop
    libreoffice
    gimp
    krita
    shotwell

    # misc

    xdg-utils
    nfs-utils


  ];
}
