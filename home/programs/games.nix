{
  pkgs,
  inputs,
  ...
}:
# games
{
 steam = {
      enable = true;
      # fix gamescope inside steam
      package = pkgs.steam.override {
        extraPkgs = pkgs:
          with pkgs; [
            keyutils
            libkrb5
            libpng
            libpulseaudio
            libvorbis
            stdenv.cc.cc.lib
            xorg.libXcursor
            xorg.libXi
            xorg.libXinerama
            xorg.libXScrnSaver
          ];
      };
    };



  home.packages = with pkgs; [
    inputs.nix-gaming.packages.${pkgs.system}.osu-lazer-bin
    gamescope
    prismlauncher
    # (lutris.override {extraPkgs = p: [p.libnghttp2];})
    winetricks
    steam
    retroarch-bare
    retroarch-assets
    retroarch-joypad-autoconfig
     (retroarch.override {
        cores = with libretro; [
          beetle-lynx
          beetle-ngp
          beetle-pce-fast
          beetle-pcfx
          beetle-psx-hw
          beetle-supergrafx
          beetle-vb
          mednafen-wswan
          blastem
          bsnes
          citra
          desmume
          doxbox
          flycast
          mame
          melonds
          mgba
          mesen
          parallel-n64
          ppsspp
          sameboy
          stella
          swanstation
          yabause
  ];
}
