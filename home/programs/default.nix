{
  lib,
  pkgs,
  ...
}: {
  imports = [

    #./games.nix
    #./git.nix
    ./gnome.nix
    ./media.nix
    ./music.nix
    ./packages.nix
    ./xdg.nix
  ];
}
