{ config, pkgs, lib, ... }:

{

  gtk = {
    enable = true;
    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };
    gtk3.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=1
      '';
    };
    gtk4.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=1
      '';
    };
  };

  # Use `dconf watch /` to track stateful changes you are doing and store them here.
  dconf.settings = {
    "org/gnome/shell" = {
      disable-user-extensions = true;
    };
    
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
      enable-hot-corners = true;
    };
  };

  home.packages = with pkgs; [

	dialect
	gnome-secrets
	eyedropper
	amberol
	gnome.gnome-terminal
	gtg
	gitg
	spot
	celluloid
	gnome.gpaste
  ];
}
