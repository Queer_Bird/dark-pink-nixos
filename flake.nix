{
  description = "NixOS configuration";

  inputs = {
    #don't touch this bit 
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    
    #provides flakes for specific hardware
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    
    #theming tool
    stylix.url = "github:danth/stylix";
    
    #adblock DNS list
    hosts.url = "github:StevenBlack/hosts";

  
  };
  
  outputs = inputs@{ nixpkgs, home-manager, nixos-hardware, stylix, hosts, self, ... }: {
          
    nixosConfigurations = {
      thinkpad = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [

          #Change this to whatever system you have. Or remove it.
          nixos-hardware.nixosModules.lenovo-thinkpad-x1-nano-gen1
          
          stylix.nixosModules.stylix
          
          #This folder has the important config stuff
          ./core
          
          hosts.nixosModule {
          networking.stevenBlackHosts = {
          #turn these off to make god cry
          enable = true;
          blockGambling = true;
          blockPorn = true;
          };
          }

          # make home-manager as a module of nixos
          # so that home-manager configuration will be deployed automatically when executing `nixos-rebuild switch`
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;

            home-manager.users.cmde = import ./home/home.nix;

            # Optionally, use home-manager.extraSpecialArgs to pass arguments to home.nix
          }
        ];
      };
    };
  };
}

