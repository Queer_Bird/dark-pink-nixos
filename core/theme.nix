{ pkgs, ... }:

{
  stylix = {
    image = ./wallpaper.jpg;
    base16Scheme = ./pinku.yaml;


    polarity = "dark";

    cursor = {
      name = "phinger-cursors-light";
      package = pkgs.phinger-cursors;
      size = 48;
      };

    fonts = {
      serif = {
        name = "Cantarell";
        package = pkgs.cantarell-fonts;
      };

      sansSerif = {
        name = "Cantarell";
        package = pkgs.cantarell-fonts;
      };

      monospace = {
        name = "Mononoki";
        package = pkgs.mononoki;
        };
        
      sizes = {        
        applications = 11;
        desktop = 11;
      };
    };    
  };
}
