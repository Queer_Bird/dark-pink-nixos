# Dark Pink Nixos

A.K.A. If GNOME had a good dark theme

![alt text](screenshot.png)

[Wallpaper by @murkety](https://twitter.com/murkety/)

## But why

I figured if I was going to have a NixOS setup, I may as well through it on the internet in case someone else wants to steal it for their own nefarious purpouses. That being said, I think I am doing something a bit unique compared to a lot of the other configs I see out there. I'll go briefly go over the things in my configuration that I think are neat.

## I am not a developer

NixOS is a very developer centric distribution, but I think it has a lot to offer to Linux users with intermediate skills. All the people who use Gentoo or some such just for cool-boy points can find a lot to enjoy from NixOS. A lot of the configurations for NixOS I see floating around are very complex and integrate with some very fancy  developement environments that I don't understand whatsoever. In the process of reverse engineering these in my efforts to learn NixOS, I quickly realized that all this was very much overkill for someone like me who does this stuff for fun. After doing this, I started over from scratch to just make something that a normal user could wrap their head around and easily modify without being hit with too many of those famous useless Nix error codes.

## Sexy dark theme

In my opinion, most dark themes on Linux desktops are seriously trash. The only one that I really am a fan of is Materia Dark. Most dark themes are at best a shade of middle gray, and I prefere almost pure black. The shade I use is #121212. Using Stylix and my custom colorscheme (pinku.yaml), I have an actually good dark theme seemlessly applied to all applications with just a splash of pink accents. The whole GNOME suite, libadwaita and all that, and pretty much anything else you can install is auto-magically matchy without even using a theme package. The Cursor it ships with is Phinger and it has Papirus Icons (daring today, aren't we?).

## How I learned to love GNOME

I went down the same rabbit hole all Linux hobbyists must go through. Try Ubuntu but quickly understand that it is not just too mainstream but also a tool of the bourgeoisis. So you start messing around with all the other distros and before you know it you are using a new tiling window manager every week because some YouTuber told you to. I spent a lot of time in DWM and Hyprland, which are awesome projects of course. NixOS might appear as just another Arch or Gentoo meme distro for people who wish they worked in tech, but no. NixOS is actually stable and it's gimmicks actually make your life easier instead (after the extreme learning curve).

You'll notice that many NixOS configs ship with a tiling window manager of some sort, especially Hyprland. I myself love Hyprland, and the first thing I did with NixOS was copy someone else's swanky Hyprland config.

But there comes a time when you don't want to maintain an elaborate unstable desktop and everyone is getting mad at you because your screenshares don't work. The simple truth is that GNOME is an amazing project and is incredibly stable when you just let GNOME be GNOME. People run into issues with GNOME when they try and force it to behave like what they are used to. The GNOME devs are incredibly opinionated, but they have a solid vision and that is why when using GNOME that is the closest the Linux desktop every comes to feeling lime a cohesive, stable experience and not a bunch of random pieces stapled together. But if you really do miss the TWM experience, the extensions "Forge" is very nice.

Though, too bad the theming support is bad. They have their design doctrines and they don't want people to break the accessability by screwing with the contrast. I can't say how well my theme would work for the visually impaired (not well I would guess) but since I am the target audience I don't suppose it matters.

## Flakes and Home-Manager

Why yes, I did include the NixOS features that they warn are unstable but at the same time mandatory to actually using the distro. I have yet to have any issues that aren't user related so I'm glad the developers are very concervative with the use of the word "stable". As a Debian fan I appriciate that.

## Other Goodies

* MPD and NCMPCPP preconfigured since I always found that to be a pain
* GAMING FLAKE LOL
* ZSH and Spaceship

## In closing

Just use Debian. Or Fedora if you simply must have newer packages lol
